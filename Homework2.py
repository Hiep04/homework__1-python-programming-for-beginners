print("====Exercise1====")
u, v, x, y, z = 29, 12, 10, 4, 3
a=u/v
print("u/v=",a)
#u/v=2.4166666666666665
t=(u==v)
print("t=",t)
#t=False
b=u%x
print("u%x=",b)
#u%x=9
t=(x>=y)
print("t=",t)
#t=True
u+=5
print("u+=5=>u=",u)
#u=34
u%=z
print("u%=z=>u=",u)
#u=1
t=(v>x and y<z)
print("t=",t)
#t=False
c=(x**z)
print("x**z=",c)
#x**z=1000
d=x//z
print("x//z=",d)
#x//z=3

print("====Exercise2====")
print("Radius of circle r=5")
import math
print("=> Area=",5**2*math.pi)
#=> Area= 78.53981633974483
print("=> Perimeter=",5*2*math.pi)
#=> Perimeter= 31.41592653589793

print("====Exercise3====")
s="Hi John, welcome to python programming for beginnerl"
#a.Check a string “python” that either exists in s or not.
print("python" in s)
#=> True ("python" string in list s)
#b.Extract the word “John” from s and save it into a variable named s1.
s1=s[3:7:1]
print("s1=",s1)
#=>s1=John
#c.Count how many character ‘o’ in s and print it on console. Guide: use count() function of string.
a="o"
print("Số lần ký tự 'o' xuất hiện=",s.count(a))
#Số lần ký tự 'o' xuất hiện= 6
#d.Count how many word in s and print it on console.
#Guide:  use split() function of string to split s to a list of strings and then use len() function to count the size of list.
b=s.split()
print('b=',b)
#b= ['Hi', 'John,', 'welcome', 'to', 'python', 'programming', 'for', 'beginnerl']
print("Số từ trong chuỗi s=",len(b))
#Số từ trong chuỗi s= 8

print("====Exerccise4====")
s=("Twinkle,twinkle,little star,\n\tHow I wonder what you are!\nUp above the world so high,\n\tLike a diamond in the"
   "sky.\nTwinkle,twinkle,little star,\n\tHow I wonder what you are.")
print(s)

print("====Exercise5====")
l=[23,4.3,4.2,31,"python",1,5.3,9,1.7]
#a.Remove the item “python” in the list.
Re=l.remove("python")
print("Re=",l)
#b.Sort this list by ascending and descending.
Ascend=l.sort()
print("ascend=",l)
Descend=l.reverse()
print("descend=",l)
#c.Check either number 4.2 to be in l or not?
print(4.2 in l)
