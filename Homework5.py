#Define a generator function (named primegen) that get a number N as a parameter and generate N prime numbers starting
# from 1.
def primegen(n):
    for num in range(1, n):
        isprime = True
        for i in range(2, num):
            if (num % i == 0):
                isprime = False
        yield num
prime = primegen(n)
print(next(prime))
#Given a string s= 'daaaddaaaaadsssssaaaaaaaddfffdaaaas'. Write a generator function, named countSeq(c, st) that gets
# 2 parameters c and st and count the sequence of character char in str
s='daaaddaaaaadsssssaaaaaaaddfffdaaaas'
def countSeq(c, s):
    a=0
    for i in range(len(s)):
        if (s[a]==c):
            a=a+1
    return a
c="a"
print(count(c,s))
#Given a list of tuple as follows. l = [(1,2,3), (2,5,3), (2,4,6,8), (3,6,4)]. Write a python script to scale down
# the values of this list into interval [0,1] in which each element of l has different scale
def fun(x):
    max_x=max(x)
    min_x=min(x)
    a=[i/(max_x+min_x) for i in x]
    return a
l = [(1,2,3), (2,5,3), (2,4,6,8), (3,6,4)]
print(list(map(fun,l)))
#Given a list as follows. l = [212,434,-32,4.3,(3,5),[2,4,7]]. Write a python script using list comprehension to
# create a list nl from l as follows. nl=[212, 434, -32, 4.3, 8, 13]
l = [212,434,-32,4.3,(3,5),[2,4,7]]
nl=[x if type (x)==int or type (x)==float else sum(x) for x in l ]
print(nl)
