print("====Exercise1====")
A={1,2,3,4,5,7}
B={2,4,5,9,12,24}
C={2,4,8}
#Iterate over all elements of set C and add each element to A and B
for setc in C:
    print(setc)
A.add(2)
A.add(4)
A.add(8)
B.add(2)
B.add(4)
B.add(4)
#Print out A and B after adding elements
print("A=",A)
print("B=",B)
#Print out the intersection of A and B
D=A.intersection(B)
print("Int A and B=",D)
#Print out the union of A and B
d=A.union(B)
print("A union B=",d)
#Print out elements in A but not in B
d=A-B
print("in A not in B=",d)
#Print out the length of A and B
d=len(A),len(B)
print("Length(A,B)=",d)
#Print out the maximum value of A union B
d=max(A.union(B))
print("Max(A union B)=",d)
#Print out the minimum value of A union B
d=min(A.union(B))
print("Min(A union B)=",d)

print("====Exercise2====")
t=(1,"python",[2,3],(4,5))
#Unpack t into 6 variables, that are: 1, 'python', 2, 3, 4, 5
a,b,[c,d],[e,f]=t
print("Unpack=",a,b,c,d,e,f)
#Print out the last element of t
print("Last element=",t[-1])
#Add to t a list [2, 3]
v=[2,3]
t=(1,"python",[2,3],(4,5)),v
print("Add v in t=",t)
#Check whether list [2, 3] is duplicated in t
a=t.count([2,3])
print("Number of occurrences of [2,3]=",a)
#Remove list [2, 3] from t
re=list(t)
for a in re:
    if a==[2,3]:
        re.remove(a)
re=tuple(re)
print('Re t=',re)
#Convert tuple t into a list
tlist=list(re)
print('list t=',tlist)

print("====Exercise3====")
dic1={1:10,2:20}
dic2={3:30,4:40}
dic3={5:50,6:60}
#3.1
dict={**dic1,**dic2,**dic3}
print('dict=',dict)
#3.2
dic4={'a':1,'b':4,'c':2}
from operator import itemgetter
dicx=sorted(dic4.items(),key=itemgetter(1))
print("dicx=",dicx)
#3.3
s="Python is an easy language to learn"
l=list(s)
def dicc(l):
    dics={}
    for item in l:
        dics[item]=l.count(item)
    print(dics)
print(dicc(l))
